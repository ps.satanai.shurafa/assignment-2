public class Stack {
    private int[] stack;
    private int count, size;
    private boolean isStatic;

    Stack(){
        size = 5;
        stack = new int[size];
        count = 0;
        isStatic = false;
    }

    Stack(int size){
        this.size = size;
        stack = new int[size];
        count = 0;
        isStatic = true;
    }

    public boolean isFull(){
        return count == size;
    }

    public boolean isEmpty(){
        return count == 0;
    }

    public void expand(){
        int[] temp = new int[size*2];

        for(int i=0; i<size; i++){
            temp[i] = stack[i];
        }

        stack = temp;
        size = size*2;
    }

    public void push(int num){
        if (isFull() && !isStatic) {
            expand();
        }
        else if (isFull() && isStatic) {
            throw new TheStackIsFull();
        }
        if (!isFull()) {
            stack[count] = num;
            count++;
        }
    }

    public void pop() {
        if(!isEmpty()){
            count--;
        }
        else throw new TheStackIsEmpty();
    }

    public int size(){
        return count;
    }

    public int peek() throws TheStackIsEmpty{
        if(!isEmpty()) {
            return stack[count-1];
        }
        else throw new TheStackIsEmpty();
    }

    public static void main(String[] args){
        Stack s = new Stack(4); // Static stack
        s.push(1);
        s.push(2);
        s.push(3);
        s.push(4);
        System.out.println(s.peek());
        //s.push(5);// Will generate an exception

        Stack s1 = new Stack(); // Dynamic stack
        s1.push(1);
        s1.push(2);
        s1.push(3);
        s1.push(4);
        s1.push(5);
        System.out.println(s1.peek());
        s1.push(5);// The stack will expand

    }

}
