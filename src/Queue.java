public class Queue {
    private int[] queue;
    private int count, size;
    private boolean isStatic;

    Queue(){
        size = 5;
        queue = new int[size];
        count = 0;
        isStatic = false;
    }

    Queue(int size){
        this.size = size;
        queue = new int[size];
        count = 0;
        isStatic = true;
    }

    public boolean isFull(){
        return count == size;
    }

    public boolean isEmpty(){
        return count == 0;
    }

    public void expandQueue(){
        int[] temp = new int[size*2];

        if (size >= 0){
            System.arraycopy(queue, 0, temp, 0, size);
        }

        queue = temp;
        size = size*2;
    }

    public void enQue(int num) {
        if (isFull() && !isStatic) {
            expandQueue();
        }
        else if (isFull() && isStatic) {
            throw new TheStackIsFull();
        }
        if (!isFull()) {
            queue[count] = num;
            count++;
        }
    }

    public void deQue() throws THeQueueIsEmpty{
        if(!isEmpty()) {
            for (int i = 0; i < count-1; i++) {
                queue[i] = queue[i + 1];
            }
            count--;
        }
        else throw new THeQueueIsEmpty();
    }

    public int size(){
        return count;
    }

    public int peek() throws THeQueueIsEmpty {
        if (!isEmpty()) {
            return queue[0];
        }
        else throw new THeQueueIsEmpty();

    }

    public static void main(String[] args){
        Queue q = new Queue(4); // Static queue
        q.enQue(1);
        q.enQue(2);
        q.enQue(3);
        q.enQue(4);
        System.out.println(q.peek());
       // q.enQue(5);// Will generate an exception

        Queue q1 = new Queue(); // Dynamic queue
        q1.enQue(1);
        q1.enQue(2);
        q1.enQue(3);
        q1.enQue(4);
        q1.enQue(5);
        System.out.println(q1.peek());
        q1.enQue(5);// The queue will expand

    }

}
